#!/usr/bin/env bash

source venv/bin/activate

time ./wiki_gitlab_backup.py 2>&1 | mail -s "Wiki GitLab Backup" sysadmin@tik.ee.ethz.ch
