# wiki gitlab backup

Searches your whole GitLab tree for wikis.

Sends the results to you via email.

# Setup

Tested on Arch Linux and Debian 9 with `python3.5` or more recent.

To install the dependencies run:
```bash
sudo apt install python3-venv git
```

To setup the environment with the `setup.sh`.

# Running

To test run the script from your terminal with the `run.sh` script.

To automate install the script in crontab.

# Troubleshooting

Sometimes git get's messed up, the savest way is to move the target directory away and sync again.
