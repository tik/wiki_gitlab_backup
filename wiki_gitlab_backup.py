#!/usr/bin/env python3

import concurrent.futures
import os
import subprocess
import sys
from time import sleep
# Install: pip install python-gitlab
import gitlab

ACCESS_TOKEN = 'hA_i5osYG1zKFn6joML7'

EXTRA_REPOS = [
    'tik/sysadmin',
]

# Must end with a /
BACKUP_DIRECTORY = '/home/tiksysnas/data/Backups/gitlab.ethz.ch/'
# Must start wit https:// and end with a /
BASE_PATH = 'https://gitlab.ethz.ch/'

# Minimal Python Version 3.5
assert sys.version_info >= (3,5)


def clone_or_update(path_with_namespace, repo_url):
    target_dir = BACKUP_DIRECTORY + path_with_namespace
    repo_name = os.path.basename(repo_url)[:-len('.git')]

    os.makedirs(target_dir, exist_ok=True)
    os.chdir(target_dir)

    exists = os.path.isdir(repo_name)

    #print('{}: exists: {}; repo_name: {};'.format(path_with_namespace, exists, repo_name))

    if exists:
        os.chdir(repo_name)
        command = ["git", "pull"]
        action = "update"
    else:
        repo_url = 'https://oauth2:{}@{}'.format(ACCESS_TOKEN, repo_url[len('https://'):])
        command = ["git", "clone", repo_url]
        action = "clone"
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    return {
        'action': action,
        'path': path_with_namespace,
        'success': result.returncode == 0,
        'stdout': result.stdout,
        'stderr': result.stderr,
    }


os.makedirs(BACKUP_DIRECTORY, exist_ok=True)

failed_jobs = 0

with gitlab.Gitlab(BASE_PATH, private_token=ACCESS_TOKEN) as gl:
    with concurrent.futures.ProcessPoolExecutor(max_workers=4) as p:
        future_to_result = []
        projects = gl.projects.list(as_list=False, owned=True, all=True)
        #projects = gl.projects.list(as_list=False, owned=True)
        i = 0
        has_avatar = 0
        for project in projects:
            if project.path_with_namespace in EXTRA_REPOS:
                future_to_result.append(p.submit(clone_or_update, project.path_with_namespace, project.http_url_to_repo))
                i += 1

            if project.wiki_enabled:
                wikis = project.wikis.list()
                if len(wikis) > 0:
                    url = project.http_url_to_repo
                    wiki_url = url[:len(url)-len('.git')] + '.wiki.git'
                    future_to_result.append(p.submit(clone_or_update, project.path_with_namespace, wiki_url))
                    i += 1
            if project.avatar_url:
                has_avatar += 1

        print('Scheduled all jobs\n    {} Repos with {} Avatars'.format(i, has_avatar))

        for future in concurrent.futures.as_completed(future_to_result):
            try:
                data = future.result()
            except Exception as exc:
                print('(EE) generated an exception: {}'.format(exc))
                failed_jobs += 1
            else:
                if data['success']:
                    print('(OK) wiki backed up {}'.format(data))
                else:
                    print('(EE) something went wrong {}'.format(data))
                    failed_jobs += 1

if failed_jobs > 0:
    print('\n(EE) Warning {} jobs failed!'.format(failed_jobs))
    sys.exit(23)
